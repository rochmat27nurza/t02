public class lcd {
    private String Status;
    private int Volume;
    private int Brightness;
    private String Cable;
    private int Cableup;


    //method
    public void turnOff(){
      Status = "Off";  
    }
    public void turnOn(){
        Status = "On";
    }
    public void freeze(){
        Status = "freeze";
    }
    public void volumeUp(){
        Volume += 2;
    }
    public void volumeDown(){
        Volume -= 1;
    }
    public void setVolume(int Volume){
        this.Volume = Volume;
    }

    public void brightnessUp(){
        Brightness += 2;
    }
    public void brightnessDown(){
        Brightness -=1;
    }
    public void setBrightness(int Brightness){
        this.Brightness = Brightness;
    }
    public void cableUp(){
        this.Cableup++;
        LCDCable();
    }
    public void cableDown(){
        this.Cableup--; 
        LCDCable();
     

    }
    public void setCable(String Cable){
       this.Cable = Cable;
    }


    
    //method output
    public void DisplayMessage(){
        System.out.println("----------- LCD ------------");
        System.out.println("Status LCD      : "  + Status);
        System.out.println("Volume LCD      : "  + Volume);
        System.out.println("Brightness LCD  : " + Brightness);
        System.out.println("Cable LCD       : " + Cable);
        System.out.println("----------------------------");
    }

    public void LCDCable(){
        switch (Cableup) {
            case 1:
                Cable = "HDMI";
                break;
            case 2:
                Cable = "VGA";
                break;
            case 3:
                Cable = "DVI";
                break;
            default:
                Cable = "Display Port";
        }

    
    }
}

        



